FROM google/cloud-sdk:343.0.0-alpine as scratch
ENV DEBIAN_FRONTEND noninteractive

FROM scratch as build
WORKDIR /tmp
RUN curl -fsSL -o helm.tar.gz https://get.helm.sh/helm-v3.6.0-linux-amd64.tar.gz
RUN tar -zxvf helm.tar.gz

FROM scratch as target
COPY --from=build /tmp/linux-amd64/helm /usr/local/bin/
RUN gcloud components install kubectl
